[[https://debian.org | Debian]] is a [[software/Linux | GNU/Linux]] distribution popular among both server and desktop computers. It's known for its old and stable package base, which is only updated with every new Debian release. Debian is also the OS of choice for most of the self-hosting guides on this wiki.

= Installing
Debian can be installed onto a device either by [[https://www.debian.org/download | downloading the latest iso]] or by using the `debootstrap` utility.

== Installing with debootstrap

Debootstrap, similarly to [[Arch Linux]]'s `pacstrap` command, is a command-line utility for installing Debian from any other existing Linux installation. It is available in the Arch repositories, meaning you can install Debian from Arch Linux.

Once you have set a partition to install Debian onto, simply run:

```
debootstrap --arch YOUR_ARCH DEBIAN_RELEASE MOUNT_POINT MIRROR
```

The parameters in capital letters have to be set by the user. For example, if one wanted to install Debian 11 into the /mnt directory using the [[http://ftp.it.debian.org/debian/ | ftp.it.debian.org]] mirror:

```
debootstrap --arch amd64 bullseye /mnt http://ftp.it.debian.org/debian/
```
= Non-free firmware and software
By default, Debian only contains free software in it's ISO; If you need to run proprietary firmware drivers for certain devices, such as processor microcode, please install Debian with the [[https://cdimage.debian.org/images/unofficial/non-free/images-including-firmware/current-live/amd64/iso-hybrid/ | non-free ISO]])//

It may also be required to alter the `/etc/apt/sources.list` file like so to enable non-free repositories if you wish to install non-free software:
```
deb YOUR_REPO buster main contrib non-free
deb-src YOUR_REPO buster main contrib non-free
```
