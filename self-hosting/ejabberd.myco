[[https://ejabberd.im | Ejabberd]] is a server for the XMPP protocol written in Erlang. It's easier to configure and setup than [[https://prosody.im | Prosody]] due to having most of its modules built-in and pre-configured by default.
# Initial server setup
This section of the guide will run through the basic installation and setup of ejabberd for minimal functionality.
## Prerequisites

* Debian 10 or newer
* NGINX installed
* Ports 80, 443 and 5222-5443 port-forwarded on your router/modem
* If you plan on using ejabberd's built-in turnserver, this will also utilize ports 3478 and 49152–65535.
* Your own domain with an A DNS entry set to the public IPv4 address of your server
* Basic UNIX knowledge

### Subdomains
Ejabberd presumes that you have already created all the **required and optional subdomains** for its operation prior to running it.

Depending on the usecase, you may need any or all of the following domains for XMPP functionality:
* **example.org** - Your XMPP hostname
* **conference.example.org** - For Multi User Chats (MUCs)
* **upload.example.org** - For file upload support
* **proxy.example.org** - For SOCKS5 proxy support
* **pubsub.example.org** - For publish-subscribe support

This guide will assume **all these subdomains** have been created.

### Custom subdomains
You can always setup **custom subdomains** for all the different modules in Ejabberd. This can be done by going to the individual module in the `/etc/ejabberd/ejabberd.yml` config file, and using the `host:` option:
```
mod_muc:
  host: muc.example.org
```
(This config changes the default `conference.example.org` domain to `muc.example.org`.)

## Installation
Ejabberd is available in the Debian repositories:
```
apt install ejabberd
```
## Configuration
The ejabberd server is configured in `/etc/ejabberd/ejabberd.yml`. Changes are only applied by restarting the ejabberd daemon in systemd:
```
systemctl restart ejabberd
```
### Hostnames
The **XMPP hostname** is specified in the `hosts` section of `ejabberd.yml`:
```
hosts:
  - example.org
```
### Certificates
Unlike [[https://prosody.im | Prosody,]] ejabberd doesn't come equipped with a script that can automatically copy over the relevant certificates to a directory where the ejabberd user can read them.

One way of organizing certificates for ejabberd is to have them stored in `/etc/ejabberd/certs`, with each domain having a separate directory for both the fullchain cert and private key.

Using certbot, this process can be easily automated with this one-liner:
```
DOMAIN=subdomain.example.org
certbot --nginx -d $DOMAIN certonly; mkdir /etc/ejabberd/certs/$DOMAIN; cp /etc/letsencrypt/live/$DOMAIN/fullchain.pem /etc/ejabberd/certs/$DOMAIN; cp /etc/letsencrypt/live/$DOMAIN/privkey.pem /etc/ejabberd/certs/$DOMAIN
```
This should be ran with your XMPP hostname (example.org) and repeated for all your desired subdomains.

After running this, ensure all the certfiles are legible by the `ejabberd` user:
```
chown -R ejabberd:ejabberd /etc/ejabberd/certs
```

To enable the use of all these certificates in ejabberd, the following configuration is necessary:
```
certfiles:
  - "/etc/ejabberd/certs/*/*.pem";
```
### Admin User

The **admin user** can be specified in `ejabberd.yml` under the `acl` section:
```
acl:
  admin:
    user: admin
```
This would make **admin@example.org** the user with administrator privileges.

## Database

### Why use a database?

In the `mod_mam` section of the ejabberd config file, the following message is in comments:

```
mod_mam:
  ## Mnesia is limited to 2GB, better to use an SQL backend
  ## For small servers SQLite is a good fit and is very easy
  ## to configure. Uncomment this when you have SQL configured:
  ## db_type: sql
```
As these comments imply, an **SQL backend** is strongly recommended if you wish to use your ejabberd server for anything more than just testing. Ejabberd supports **MySQL, SQLite** and **PostgreSQL.**

While all of those are suitable choices, the best database system to use is PostgreSQL. It's the same database backend used by [[self-hosting/peertube | PeerTube]] and [[self-hosting/matrix | Matrix,]] making it the most convenient option if you're already running those too.

### Installing PostgreSQL

PostgreSQL is available in the Debian repositories:
```
apt install postgresql
```
Start the PostgreSQL daemon to begin using it:
```
systemctl start postgresql
```
### Creating the Database
To create the database, first create a PostgreSQL user for ejabberd:
```
su -c &quot;createuser --pwprompt ejabberd&quot; postgres
```
Then, create the database and make `ejabberd` its owner:
```
su -c "psql -c 'CREATE DATABASE ejabberd OWNER ejabberd' " postgres
```
### Importing Database Scheme
Ejabberd doesn't create the database scheme by default; It has to be imported into the database before use.
```
su -c &quot;curl -s https://raw.githubusercontent.com/processone/ejabberd/master/sql/pg.sql | psql ejabberd&quot; postgres
```
### Configuring ejabberd to use PostgreSQL
Finally, add the following configuration to `ejabberd.yml`:
```
default_db: sql
sql_type: pgsql
sql_server: &quot;localhost&quot;
sql_database: &quot;ejabberd&quot;
sql_username: &quot;ejabberd&quot;
sql_password: &quot;psql_password&quot;
```
Once you've ensured your database name, username and password are all correct, enable SQL storage for `mod_mam`:
```
mod_mam:
  ## (Other parameters)
  db_type: sql
```
# Using ejabberd
## Registering the Admin User

To begin using ejabberd, firstly start the ejabberd daemon:
```
systemctl restart ejabberd
```
Then, using `ejabberdctl` as the ejabberd user, register the admin user which is set in `ejabberd.yml`:
```
su -c &quot;ejabberdctl register admin example.org password&quot; ejabberd
```
This will create the user **admin@example.org.**
## Using the Web Interface

By default, ejabberd has a web interface accessible from `http://example.org:5280/admin`. When accessing this interface, you will be prompted for the admin credentials:
img{images/ejabberd-login}

After signing in with the admin credentials, you will be able to manage your ejabberd server from this web interface:
img{images/ejabberd-admin}

## TURN &amp; STUN for Calls

Ejabberd supports the **TURN** and **STUN** protocols to allow internet users behind NATs to perform voice and video calls with other XMPP users.

**//If you're running ejabberd on a system which isn't behind an NAT (like a VPS), then setting up a separate coturn server is optional as ejabberd already contains one. The following steps are only for users that run their ejabberd server behind an NAT, but have access to a separate server to act as their TURN server.//**

Firstly, setup a TURN and STUN server with [[self-hosting/coturn|Coturn,]] using an **authentication secret.**

Then, edit `mod_stun_disco` to contain the appropriate information for your turnserver:
```
  mod_stun_disco:
    secret: "your_shared_secret"
    services:
    -
      host: turn.example.org
      type: stun
    -
      host: turn.example.org
      type: turn
```
And with that, you've successfully setup your ejabberd XMPP server!