[[https://www.mongodb.com/ | MongoDB]] is a modern, document-based database that is used by a lot of web software and services. Unlike SQL databases such as [[SQL | MySQL]] that store data in tables, MongoDB utilizes json documents instead.

= Initial Setup
This section covers the base installation of a MongoDB database server; It is highly recommended to follow the steps in [[MongoDB#Further Setup | further setup.]]

== Prerequisites
* A VPS or local server for self-hosting
* An installation of [[Self-Hosting/Debian | Debian 10]]
* Basic UNIX knowledge

== Installation
To install a relatively modern version of MongoDB on Debian, it is recommended to use the [[https://docs.mongodb.com/manual/tutorial/install-mongodb-on-debian/ | MongoDB community edition repositories:]]
```
wget -qO - https://www.mongodb.org/static/pgp/server-4.4.asc | sudo apt-key add -
echo "deb http://repo.mongodb.org/apt/debian buster/mongodb-org/4.4 main" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.4.list
```
Then update your repository database:
```
sudo apt update
```
And install MongoDB:
```
sudo apt install mongodb-org
```
== Systemd Service
The `mongodb-org` package automatically installs a [[systemd]] service that you can run to start, stop or enable MongoDB at launch:
```
sudo systemctl restart mongodb
```
Enabling MongoDB at launch:
```
sudo systemctl enable mongodb
```
== Configuration
Configuration of the MongoDB daemon can be done in `/etc/mongod.conf`;
Otherwise, a specific configuration file can be specified with the `--config` option while using `mongod`

//(Note: It is recommended to run `mongod` as the user `mongodb`, which is created when installing MongoDB.)//

= Further Configuration
There are some further recommended steps to take to ensure secure functioning of the MongoDB database:

== Creating a database
To create a database, first access the MongoDB shell:
```
mongo
```
Then use the `use` command to create (if not already existing) and begin using a database:
```
use DATABASE
```
== Enabling Authentication/Access Control
MongoDB supports authentication, but it is disabled by default; To enable it, first enter the MongoDB shell and create a user named `MyUserAdmin` under the `admin` database:
```
mongo
use admin
db.createUser(
  {
    user: "myUserAdmin",
    pwd: passwordPrompt(),
    roles: [ { role: "userAdminAnyDatabase", db: "admin" }, "readWriteAnyDatabase" ]
  }
)
```
This will prompt for a password for the admin user; **Remember that password!**

Now exit the MongoDB shell and edit the daemon configuration file at `/etc/mongod.conf` and add the following line under the `security` section:
```
security:
    authorization: enabled
```

Restart the daemon with `sudo systemctl restart mongod` and test authentication:
```
mongo --authenticationDatabase "admin" -u "myUserAdmin" -p
```
This will prompt for your password; Enter it, and if you are able to enter MongoDB, you've enabled authentication!

=== Creating a new user
To create a new user, first specify their authentication database:
```
use DATABASE
```
**(Note: [[https://gitgud.io/fatchan/jschan | some software]] may specifically require you to use the `admin` database to authenticate.)**

Then create the user:
```
db.createUser(
  {
    user: "USER",
    pwd:  passwordPrompt(),
    roles: [ { role: "readWrite", db: "DATABASE" },
             { role: "read", db: "reporting" } ]
  }
)
```

With this, a user is created.