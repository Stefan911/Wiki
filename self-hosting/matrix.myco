img{images/matrix | 400}

[[https://matrix.org | Matrix]] is a free/libre API and protocol for chat and voice communication; Think DIY Discord, except without all the spyware. One can setup their own Matrix homeserver using either the [[https://github.com/matrix-org/synapse | Matrix-Synapse]] or [[https://github.com/matrix-org/dendrite | Dendrite]] software; A Matrix server supports messaging, encrypted messaging, voice and video calls (once a turn server is set up), and many other services.

= Initial Server Setup
This section covers base installation and setup of Matrix-Synapse, about enough to get the homeserver running; If you wish to actually register an account to the instance and use it, consult the further server configuration part.

== Prerequisites
* Debian 10 or newer
* [[self-hosting/nginx | NGINX]] installed
* Ports 80, 443 and 8448 port-forwarded on your router/modem
* Your own domain with an A DNS entry set to the public IPv4 address of your server
* Basic UNIX knowledge

== Webserver Configuration
Before you install any packages, there has to be an appropriate configuration for Matrix-Synapse in your `/etc/nginx/sites-available` directory. Here's an example configuration:

```
server {
        server_name YOUR_DOMAIN;

        listen 80;
        listen [::]:80;

        listen 443 ssl http2 default_server;
        listen [::]:443 ssl http2 default_server;
       
        listen 8448 ssl http2 default_server;
        listen [::]:8448 ssl http2 default_server; 
	
	location ~* ^(\/_matrix|\/_synapse\/client) {
        proxy_pass http://localhost:8008;
        proxy_set_header X-Forwarded-For $remote_addr;
        client_max_body_size 50M;
        }

	location /.well-known/matrix/client {
        return 200 '{"m.homeserver": {"base_url": "https://YOUR_DOMAIN"}}';
        default_type application/json;
        add_header Access-Control-Allow-Origin *;
        }

        location /.well-known/matrix/server {
        return 200 '{"m.server": "YOUR_DOMAIN:443"}';
        default_type application/json;
        add_header Access-Control-Allow-Origin *;
        }
}
```

**(This example configuration file contains all configuration needed to utilize both federation and client Well-Known URI)**

This configuration file should be placed in `/etc/nginx/sites-available` and then symbolically linked to `/etc/nginx/sites-enabled`:
```
sudo ln -s /etc/nginx/sites-available/YOUR_DOMAIN /etc/nginx/sites-enabled
```
== Installing packages
To actually install Matrix-Synapse on Debian, first add the official Matrix Debian repo to your system:
``` 
sudo apt install -y lsb-release wget apt-transport-https
sudo wget -O /usr/share/keyrings/matrix-org-archive-keyring.gpg https://packages.matrix.org/debian/matrix-org-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/matrix-org-archive-keyring.gpg] https://packages.matrix.org/debian/ $(lsb_release -cs) main" |
    sudo tee /etc/apt/sources.list.d/matrix-org.list
```

Then update the repository database and install the `matrix-synapse-py3` package:
```
sudo apt update
sudo apt install matrix-synapse-py3
```
Now simply reload the following services with <code>systemctl</code>:

 sudo systemctl restart nginx matrix-synapse

And you've done it! Matrix-Synapse is now successfully installed;

= Further Server Configuration
Now that the Matrix-Synapse server has been installed and some basic configuration has been done, it's highly recommended to further configure it to enable things such as SSL encryption, a PostgreSQL database, and user registration.

== Encryption with Certbot

Begin by installing Certbot to your Debian system, along with the NGINX extension:
```
sudo apt install python3-certbot python3-certbot-nginx
```
As shown in the [[self-hosting/nginx | NGINX]] page on this wiki, Let's Encrypt's Certbot tool can be used to not only generate an encryption certificate for your domain, but also write the appropriate configuration to the server entry in `/etc/nginx/sites-available`.

We can do all this with the following command:
```
 sudo certbot --nginx -d YOUR_DOMAIN
```
And success! You've generated an encryption certificate and enabled encryption for your Matrix homeserver.

== Enabling PostgreSQL
It's highly recommended to use PostgreSQL as the database for your Matrix homeserver instead of the default SQLite database configuration; To set this up, begin by installing PostgreSQL:
```
sudo apt install postgresql
```
Now start the daemon:
```
sudo systemctl start postgresql
```
Now to create a database for your Matrix homeserver, first switch to the `postgres` user:
```
sudo su - postgres
```
//(If the user has no password, create one for it by using <code>sudo passwd postgres</code>)//

Now create a new PostgreSQL user, in this case named `synapse_user`:
```
createuser --pwprompt synapse_user
```
Now run the `>psql` command to access the PostgreSQL shell interface, and create a new database named `synapse`, setting `synapse_user` as it's owner:
```
psql
CREATE DATABASE synapse
 ENCODING 'UTF8'
 LC_COLLATE='C'
 LC_CTYPE='C'
 template=template0
 OWNER synapse_user;
```

Now edit the database configuration in `/etc/matrix-synapse/homeserver.yaml` and comment out the following lines for the previous SQLite configuration:
```
# database:
  # name: sqlite3
  # args:
    # database: DATADIR/homeserver.db
```
//(Note: The example above is how yours should look like after it's commented out.)//

Then, uncomment the following configuration above, and set the appropriate entries:
```
database:
  name: psycopg2
  args:
    user: synapse_user
    password: secretpassword
    database: synapse
    host: localhost
    cp_min: 5
    cp_max: 10
```

Ensure that `synapse` is set to your database name, `synapse_user` is set to that database's owner, and that `secretpassword` is set to that user's password.

Now restart the `martix-synapse` service:
```
sudo systemctl restart matrix-synapse
```
And congratulations! Your Matrix homeserver now uses a PostgreSQL database!

## Enabling user registration

Enabling user registration means anyone will be able to register a Matrix account to your instance; This includes yourself, and enabling this feature is required to register an admin account. **Please note, Matrix accounts cannot be deleted, only "deactivated".**

Enabling registration is as simple as uncommenting and changing one line in the `/etc/matrix-synapse/homeserver.yaml` file:
```
enable_registration: true
```
To be able to create users from your homeserver with the `register_new_matrix_user`  command, you also need to uncomment this line in your matrix config:
```
 registration_shared_secret: ???
```
Then simply restart Matrix-Synapse with:
```
sudo systemctl restart matrix-synapse
```
Now, use the `register_new_matrix_user` command, which is installed alongside the `matrix-synapse-py3` package, to create a new admin user on your server:
```
sudo register_new_matrix_user -u YOUR_USERNAME -p YOUR_PASSWORD -a -c /etc/matrix-synapse/homeserver.yaml https://YOUR_DOMAIN:8448
```
//(If you are running this on your own server, there is no need to add the `https://YOUR_DOMAIN:8448` part of this command.)//

== Client Well-Known URI
An optional but recommended aspect to configure is the client Well-Known URI, which allows any user to simply type in their matrix username, for example <code>@denshi:matrix.denshi.live</code>, into any Matrix client, and have this automatically resolve their homeserver, skipping the homeserver setting step entirely;

This is already setup in the NGINX configuration previously stated in this guide.

== Federation
As mentioned before in the Initial Setup section of this page, the [[self-hosting/nginx | NGINX]] example configuration contains all the lines needed for federation to successfully work under it; No further configuration necessary!

You can test the federation [[https://federationtester.matrix.org | here.]]

However, some extra features can be enabled to increase the usability of your homeserver over federation. In `homeserver.yaml`, the following lines can be edited:
```
allow_public_rooms_over_federation: true
```
This can be un-commented to allow users to add your homserver to their list of servers (in a client like Element) and see a list of all the public rooms.
```
allow_public_rooms_without_auth: true
```
This can be un-commented to enable guests to see public rooms without authenticating.

== URL Previews
Matrix-Synapse supports URL previews; To enable them, change this line to `true` in `/etc/matrix-synapse/homeserver.yaml`:
```
url_preview_enabled: true
```
And make sure to uncomment the `url_preview_ip_range_blacklist:` section; **Otherwise, Synapse will refuse to start up again!**

= Client Recommendations
The most popular client is [https://element.io Element] but its user interface might not be what everyone is used to. [https://schildi.chat Schildi Chat] is a fork of Element to have a user interface that resembles the user interface of traditional instant messangers like whatsapp and telegram. Some people might prefer this.
