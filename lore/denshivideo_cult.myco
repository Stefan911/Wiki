The Denshi Cult (officially **DenshiVideo "Cult"** and formerly **DenshiDiscord**), are a group of interconnected Discord, XMPP and Matrix chat rooms dedicated and created for the [[people/denshi | Denshi]] YouTube channels. The chat rooms have been through 3 different deletions, various raids and boycotts throughout it's existence. [[https://denshi.org/discord | They still exist to this day.]]

= Content
img{images/denshi-cult-screenshot {The `#rules` page of the server, as seen on Discord.}}
The DenshiVideo chats consist of 3 publicly accessible text channels: **`#generalic-chat`** for general talk, **`#tech-support`** for asking tech-related questions and **`#bloated-meme-chat`** for zoomers to post their [[lore/bloat | bloated]] memes.

There used to be a music channel (**`#bloated-music-chat`**) but it was removed in favor of **`#bloated-crypto-chat`**, which was also later removed on the 22nd of August, 2021.

= History
The original DenshiVideo "Cult" was first created in early 2020 as essentially a question and answer forum and video suggestion platform for [[people/denshi | Denshi]]. It has experienced a surprisingly troubled and controversial history for being an incredibly small guild on Discord.

== Creation and move to Steam
img{images/denshi-cult-average-conversation}
The guild's initial form, "Denshi's Cult" was only used for posting videos, and asking and answering questions with Denshi. Eventually, **`#tech-support`** was added to the guild as a tech discussion and help channel. A Discord voice channel existed named "Voice for confident people" but it was rarely used. The guild gained various members which are still present on the modern day, 3rd reincarnation of the guild.

It was decided in a rash decision by Denshi to move everyone on the guild to a new platform, namely Steam chat. This was motivated by Valve's more friendly approach to Linux, and the fact that Discord's services were blocked or difficult to use in the United Arab Emirates. The initial plan was to move to Tox, but this was rejected by the community due to Tox's lacking functionality in terms of saving conversations. After everyone had transitioned to Steam chat, the guild was kept available but read-only for "archival purposes" which was just a fancy excuse for recycling memes.

== Deepfakes and Denshi
Deepfakes were once a large part of the culture of DenshiVideo "Cult" as an active member used to make deepfakes of Denshi's face singing songs and lip-syncing to audio from videos from OpenBSD-focussed YouTuber, Paranoid Life. These memes were rarely posted by anybody else other than the active member, so when he left the server in late 2020 the memes died almost instantly.

== First deletion
Eventually there was a ''very democratic'' vote held on the Denshi Steam chat to possibly go back to Discord as the platform of group communication, and the majority of members voted yes. This prompted the creation of an entirely new guild to "start fresh" as Denshi put it, yet another excuse to bury the past. The original guild was deleted, and by that I mean hidden as we all know that Discord's privacy policy probably ensures that none of your data will ever be truly gone.

== Second deletion
img{images/democratic-propaganda}
The guild suffered a second deletion after Denshi had yet another rant on Vitanam's guild, this time about the Team Fortress 2 April 2020 source code leak. His asinine argument being that if games were open source, there wouldn't be this issue. He got so heated about it that this caused him to not only be banned from the Vitanam guild, but also delete DenshiDiscord and leave any previous Discord guilds he had partaken in. He came back crawling once again after his Discord friends asked him to reincarnate the guild once again.

== Third deletion
The Discord guild underwent another deletion after Denshi had an Autistic fit over the Zoomer meme "pog". This was mostly motivated by Denshi's rampant and seemingly unjustified hatred for Zoomers and their culture. ''I know.'' The guild came back into existence after once again popular demand required it to be so.

== First "crisis" (Brando Krisis)
The first crisis is described as the period when Denshi gave away ownership of the guild to [[Diego Brando]] and underwent an identity crisis. Denshi deleted all channels on the guild, besides #tech-support, believing that the guild's identity should remain closer to his YouTube channel and not develop as it's own microcosm of culture. During this time his Discord username changed multiple times, and the channel was rebranded. He came back to the guild and regained ownership after a good 5 hours of not partaking in the guild.

== Second "crisis" (Idiot Krisis)
Denshi freaked out once again, this time at Randy after having an argument about religion. This concluded with the guild's chats being deleted once again and later being recreated to purge the history, with Randy being branded as a Bugman, and Denshi as an intolerant boomer.

== Third "crisis" (Second Migration Krisis)
Denshi was asked for his age on Discord, giving his date of birth confirming he's 14. Discord, in their infinite wisdom, took his account away and forced Denshi to submit a picture with him holding his legal ID and a piece of paper with his Discord username. A temporary guild was put in place named the "DenshiDiscord (Crisis Mode)".

Eventually Discord tech support, in their infinite and indisputable wisdom, decided to irreversibly delete Denshi's original account (Denshi#9861) giving no option to further appeal or reinstate the account. The DenshiDiscord (Crisis Mode) was converted into the current, 5th iteration of the DenshiDiscord guild, and as of the time of writing is the latest and operational DenshiDiscord guild, linked in every DenshiVideo video.

Sometime in October 2020, the guild was renamed to DenshiVideo Cult in reference to its former name to coincide with the revival of old Denshi.

== Fourth "crisis" (Generalic Krisis)
img{images/generalic-pre-crisis}
On the 28th of June, 2021, the guild entered another crisis when Discord informed Denshi that the guild had violated their rules regarding "malicious sharing of the personal or private information of other users". This had occurred after a user filed a report against users in the guild. Accusations of the former user Kowater being a pedophile, said user's alleged personal information/dox, and attempts to harass the user and his alleged classmates were included in the report. Discord threatened to take further action if the original messages weren't removed.

To oblige, the entirety of the Generalic chat was removed. An archive was made by welt of the channel as well as the rest of the guild up to that point. You can download it [[https://archive.org/details/denshidiscord-21-06-28 | here]].